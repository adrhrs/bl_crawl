package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

func getToken() (token string) {
	resx, err := http.Get("https://www.bukalapak.com/c?from=navbar_icon")
	if err != nil {
		log.Fatal(err)
	}
	defer resx.Body.Close()

	doc, err := goquery.NewDocumentFromReader(resx.Body)
	if err != nil {
		log.Fatal(err)
	}

	doc.Find("meta").Each(func(i int, sel *goquery.Selection) {
		name, _ := sel.Attr("name")
		if name == "oauth-access-token" {
			token, _ = sel.Attr("content")
		}
	})

	return
}

func crawl(cat string, page int, tk string) (result Res) {

	offset := strconv.Itoa(page * 50)

	url := "https://api.bukalapak.com/products?access_token=" + tk + "&limit=50&offset=" + offset + "&category_id=" + cat

	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("authority", "api.bukalapak.com")
	req.Header.Add("accept", "application/vnd.bukalapak.v4+json")
	req.Header.Add("sec-fetch-dest", "empty")
	req.Header.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36")
	req.Header.Add("origin", "https://www.bukalapak.com")
	req.Header.Add("sec-fetch-site", "same-site")
	req.Header.Add("sec-fetch-mode", "cors")
	req.Header.Add("referer", "https://www.bukalapak.com/")
	req.Header.Add("accept-language", "en-US,en;q=0.9,id;q=0.8,ar;q=0.7")

	res, err := client.Do(req)
	// body, err := ioutil.ReadAll(res.Body)
	// fmt.Println(string(body))

	if err != nil {
		fmt.Println(err)
	} else {
		json.NewDecoder(res.Body).Decode(&result)
		// fmt.Println(" -- status", result.Meta.HTTPStatus)
	}

	return
}

func upsert(d Data) (resp EsResp) {

	t := time.Now()
	formatted := fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02d",
		t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())

	d.UpdatedAt = formatted

	dt, _ := json.Marshal(d)

	url := "http://localhost:9200/bl-products/_doc/" + strconv.Itoa(d.SkuID)
	method := "POST"

	payload := strings.NewReader(string(dt))

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}

	json.NewDecoder(res.Body).Decode(&resp)

	return
}

func upsertHist(dx Data) (resp EsResp) {

	d := Hist{}
	d.Active = dx.Active
	d.Name = dx.Name
	d.SkuID = dx.SkuID
	d.Price = dx.Price
	d.Deal = dx.Deal
	d.SpecialCampaignID = dx.SpecialCampaignID
	d.Stats = dx.Stats
	d.Stock = dx.Stock
	d.Store.ID = dx.Store.ID
	d.Store.Name = dx.Store.Name
	d.Store.Name = dx.Store.Name
	d.Store.Reviews = dx.Store.Reviews
	d.Store.SubscriberAmount = dx.Store.SubscriberAmount

	t := time.Now()
	formatted := fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02d",
		t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())

	d.UpdatedAt = formatted

	dt, _ := json.Marshal(d)

	url := "http://localhost:9200/bl-products-hist/_doc"
	method := "POST"

	payload := strings.NewReader(string(dt))

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}

	json.NewDecoder(res.Body).Decode(&resp)

	return

}

func crawlWorker(id int, jobs <-chan Param, results chan<- Res) {

	for j := range jobs {

		t1 := time.Now()

		catID := j.CatID
		page := j.Page
		tkn := j.Token

		fmt.Println("crawler ", id, page)

		tempRes := crawl(catID, page, tkn)

		t2 := time.Now()

		fmt.Println("crawler ", id, page, "done crawling in ", t2.Sub(t1), len(tempRes.Data))

		successCount := 0
		successCountHist := 0
		for _, y := range tempRes.Data {

			res := upsert(y)
			if res.Shards.Failed == 0 {
				successCount += 1
			}
			res = upsertHist(y)
			if res.Shards.Failed == 0 {
				successCountHist += 1
			}

		}

		t3 := time.Now()

		fmt.Println("crawler ", id, page, "done storing in ", t3.Sub(t2), successCount, successCount, len(tempRes.Data))

		results <- tempRes
	}

}

func mainCrawl() {

	t1 := time.Now()

	cat := "471"
	tkn := getToken()

	wanted := 100000
	limit := 50
	pages := wanted / limit

	numJobs := pages
	jobs := make(chan Param, numJobs)
	results := make(chan Res, numJobs)
	res := []Res{}

	for w := 1; w <= 16; w++ {
		go crawlWorker(w, jobs, results)
	}

	for j := 0; j < numJobs; j++ {
		p := Param{
			Token: tkn,
			CatID: cat,
			Page:  j,
		}
		jobs <- p
	}
	close(jobs)

	for a := 1; a <= numJobs; a++ {
		res = append(res, <-results)
	}

	fmt.Println("---", len(res))
	totAll := 0

	t2 := time.Now()
	elapsed := t2.Sub(t1)

	for _, x := range res {

		tot := len(x.Data)
		totAll += tot
	}

	fmt.Println(totAll, elapsed)

}

func getProductResult() (resp EsData) {

	url := "http://localhost:9200/bl-products/_search"
	method := "POST"

	payload := strings.NewReader("{\n	\"size\":50000,\n	\"query\": {\n		\"match_all\": {}\n	}\n}")

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)

	json.NewDecoder(res.Body).Decode(&resp)

	return

}

func doCrawlReview(tkn, id, off string) (resp ReviewRespBL) {
	url := "https://api.bukalapak.com/product-reviews?access_token=" + tkn + "&product_id=" + id + "&offset=" + off + "&limit=80"
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("authority", "api.bukalapak.com")
	req.Header.Add("accept", "*/*")
	req.Header.Add("sec-fetch-dest", "empty")
	req.Header.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36")
	req.Header.Add("origin", "https://m.bukalapak.com")
	req.Header.Add("sec-fetch-site", "same-site")
	req.Header.Add("sec-fetch-mode", "cors")
	req.Header.Add("referer", "https://m.bukalapak.com/reviews/komputer/aksesoris-226/charger-1513/17wdff3-jual-adaptor-charger-original-laptop-asus-a450-a450c-a450ca-a450cc-a450v-a450vb-a450vc-a450ve-a450l-a450la-a450lb-a450lc-a451-a451l-a451la-a451lb-a451lc")
	req.Header.Add("accept-language", "en-US,en;q=0.9,id;q=0.8,ar;q=0.7")

	res, err := client.Do(req)

	json.NewDecoder(res.Body).Decode(&resp)

	return

}

func crawlReview(tkn, id string) (resp ReviewRespBL) {

	init := doCrawlReview(tkn, id, "0")
	tot := float64(init.Meta.Total)
	limit := float64(80)

	resp.Meta = init.Meta

	off := math.Ceil(tot / limit)
	reviews := []DataReview{}
	var i float64

	for i = 0; i < off; i++ {
		p := strconv.Itoa(int(i))
		revs := doCrawlReview(tkn, id, p)
		reviews = append(reviews, revs.Data...)
	}

	resp.Data = reviews

	return
}

func workerCrawlReview(id int, jobs <-chan ParamReview, results chan<- ReviewRespBL) {

	for j := range jobs {

		t1 := time.Now()
		tempRes := crawlReview(j.Token, j.ID)
		t2 := time.Now()
		fmt.Println("worker", id, "reviews", j.ID, "[FINISH]", len(tempRes.Data), t2.Sub(t1))

		tot := 0
		for _, d := range tempRes.Data {
			upsertReview(d)
			tot++
		}
		t3 := time.Now()
		fmt.Println("worker", id, "reviews", j.ID, "[STORED]", len(tempRes.Data), tot, t3.Sub(t2))

		results <- tempRes

	}
	return
}

func upsertReview(rev DataReview) (resp EsResp) {

	data, _ := json.Marshal(rev)

	url := "http://localhost:9200/bl-product-reviews/_doc/" + strconv.Itoa(rev.ID)
	method := "POST"

	payload := strings.NewReader(string(data))

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	json.NewDecoder(res.Body).Decode(&resp)
	return
}
func mainCrawlReview(blProd EsData) {

	t1 := time.Now()
	blIDs := []string{}
	for _, x := range blProd.Hits.Hits {
		id := x.Source.ID
		blIDs = append(blIDs, id)
	}

	tkn := getToken()

	numJobs := len(blIDs)
	jobs := make(chan ParamReview, numJobs)
	results := make(chan ReviewRespBL, numJobs)
	res := []ReviewRespBL{}

	for w := 1; w <= 16; w++ {
		go workerCrawlReview(w, jobs, results)
	}

	for j := 0; j < numJobs; j++ {
		p := ParamReview{
			Token: tkn,
			ID:    blIDs[j],
		}
		jobs <- p
	}
	close(jobs)
	totRev := 0
	for a := 1; a <= numJobs; a++ {
		r := <-results
		res = append(res, r)
		totRev += len(r.Data)
	}

	t2 := time.Now()

	fmt.Println(totRev, t2.Sub(t1))

}
func main() {

	mainCrawl()

	fmt.Println("getting from es")
	blProd := getProductResult()
	fmt.Println("start crawling reviews")
	mainCrawlReview(blProd)

}
