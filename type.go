package main

import "time"

type Res struct {
	Data []Data `json:"data"`
	Meta struct {
		FacetCacheID interface{}   `json:"facet_cache_id"`
		Facets       []interface{} `json:"facets"`
		HTTPStatus   int           `json:"http_status"`
		Limit        int           `json:"limit"`
		Offset       int           `json:"offset"`
		PriceGroups  []interface{} `json:"price_groups"`
		PriceRange   struct {
			Max int `json:"max"`
			Min int `json:"min"`
		} `json:"price_range"`
		Suggestions interface{} `json:"suggestions"`
		Total       int         `json:"total"`
	} `json:"meta"`
}

type Param struct {
	Token string
	CatID string
	Page  int
}

type Data struct {
	Active             bool          `json:"active"`
	Assurance          bool          `json:"assurance"`
	AvailableCountries []interface{} `json:"available_countries"`
	Category           struct {
		ID        int      `json:"id"`
		Name      string   `json:"name"`
		Structure []string `json:"structure"`
		URL       string   `json:"url"`
	} `json:"category"`
	Condition string    `json:"condition"`
	CreatedAt time.Time `json:"created_at"`
	Deal      struct {
		AppliedDate   time.Time `json:"applied_date"`
		DiscountPrice int       `json:"discount_price"`
		ExpiredDate   time.Time `json:"expired_date"`
		OriginalPrice int       `json:"original_price"`
		Percentage    int       `json:"percentage"`
	} `json:"deal,omitempty"`
	DefaultCatalog interface{} `json:"default_catalog"`
	Description    string      `json:"description"`
	ForSale        bool        `json:"for_sale"`
	ID             string      `json:"id"`
	Images         struct {
		LargeUrls []string `json:"large_urls"`
		SmallUrls []string `json:"small_urls"`
	} `json:"images"`
	Imported     bool          `json:"imported"`
	Installments []interface{} `json:"installments"`
	MaxQuantity  int64         `json:"max_quantity"`
	MinQuantity  int           `json:"min_quantity"`
	Name         string        `json:"name"`
	Price        int           `json:"price"`
	ProductSin   []interface{} `json:"product_sin"`
	Rating       struct {
		AverageRate float64 `json:"average_rate"`
		UserCount   int     `json:"user_count"`
	} `json:"rating"`
	RelistedAt   time.Time `json:"relisted_at"`
	RushDelivery bool      `json:"rush_delivery"`
	Shipping     struct {
		ForceInsurance       bool          `json:"force_insurance"`
		FreeShippingCoverage []interface{} `json:"free_shipping_coverage"`
	} `json:"shipping"`
	SkuID int `json:"sku_id"`
	SLA   struct {
		Type  string `json:"type"`
		Value int    `json:"value"`
	} `json:"sla"`
	SpecialCampaignID int `json:"special_campaign_id"`
	Specs             struct {
		Brand string `json:"brand"`
	} `json:"specs"`
	State            string        `json:"state"`
	StateDescription []interface{} `json:"state_description"`
	Stats            struct {
		InterestCount       int `json:"interest_count"`
		SoldCount           int `json:"sold_count"`
		ViewCount           int `json:"view_count"`
		WaitingPaymentCount int `json:"waiting_payment_count"`
	} `json:"stats"`
	Stock int `json:"stock"`
	Store struct {
		Address struct {
			City     string `json:"city"`
			Province string `json:"province"`
		} `json:"address"`
		Alert       string   `json:"alert"`
		AvatarURL   string   `json:"avatar_url"`
		BrandSeller bool     `json:"brand_seller"`
		Carriers    []string `json:"carriers"`
		Closing     struct {
			Closed bool `json:"closed"`
		} `json:"closing"`
		DeliveryTime string        `json:"delivery_time"`
		Description  string        `json:"description"`
		Flagship     bool          `json:"flagship"`
		Groups       []interface{} `json:"groups"`
		HeaderImage  struct {
			URL string `json:"url"`
		} `json:"header_image"`
		ID         int `json:"id"`
		Inactivity struct {
			Inactive     bool      `json:"inactive"`
			LastAppearAt time.Time `json:"last_appear_at"`
		} `json:"inactivity"`
		LastOrderSchedule struct {
			Friday    string `json:"friday"`
			Monday    string `json:"monday"`
			Saturday  string `json:"saturday"`
			Thursday  string `json:"thursday"`
			Tuesday   string `json:"tuesday"`
			Wednesday string `json:"wednesday"`
		} `json:"last_order_schedule"`
		Level struct {
			ImageURL string `json:"image_url"`
			Name     string `json:"name"`
		} `json:"level"`
		Name             string `json:"name"`
		PremiumLevel     string `json:"premium_level"`
		PremiumTopSeller bool   `json:"premium_top_seller"`
		Rejection        struct {
			RecentTransactions int `json:"recent_transactions"`
			Rejected           int `json:"rejected"`
		} `json:"rejection"`
		Reviews struct {
			Negative int `json:"negative"`
			Positive int `json:"positive"`
		} `json:"reviews"`
		SLA struct {
			Type  string `json:"type"`
			Value int    `json:"value"`
		} `json:"sla"`
		SubscriberAmount int    `json:"subscriber_amount"`
		TermAndCondition string `json:"term_and_condition"`
		URL              string `json:"url"`
	} `json:"store"`
	TagPages []struct {
		Name string `json:"name"`
		URL  string `json:"url"`
	} `json:"tag_pages"`
	URL      string `json:"url"`
	VideoURL string `json:"video_url"`
	Warranty struct {
		Cheapest bool `json:"cheapest"`
	} `json:"warranty"`
	Weight     int           `json:"weight"`
	Wholesales []interface{} `json:"wholesales"`
	UpdatedAt  string        `json:"updated_at"`
}

type Hist struct {
	SkuID  int    `json:"sku_id"`
	Name   string `json:"name"`
	Active bool   `json:"active"`
	Price  int    `json:"price"`
	Deal   struct {
		AppliedDate   time.Time `json:"applied_date"`
		DiscountPrice int       `json:"discount_price"`
		ExpiredDate   time.Time `json:"expired_date"`
		OriginalPrice int       `json:"original_price"`
		Percentage    int       `json:"percentage"`
	} `json:"deal,omitempty"`
	SpecialCampaignID int `json:"special_campaign_id"`
	Rating            struct {
		AverageRate float64 `json:"average_rate"`
		UserCount   int     `json:"user_count"`
	} `json:"rating"`
	Stats struct {
		InterestCount       int `json:"interest_count"`
		SoldCount           int `json:"sold_count"`
		ViewCount           int `json:"view_count"`
		WaitingPaymentCount int `json:"waiting_payment_count"`
	} `json:"stats"`
	Stock int `json:"stock"`
	Store struct {
		ID      int    `json:"id"`
		Name    string `json:"name"`
		Reviews struct {
			Negative int `json:"negative"`
			Positive int `json:"positive"`
		} `json:"reviews"`
		SubscriberAmount int `json:"subscriber_amount"`
	} `json:"store"`
	UpdatedAt string `json:"updated_at"`
}

type EsResp struct {
	Index   string `json:"_index"`
	Type    string `json:"_type"`
	ID      string `json:"_id"`
	Version int    `json:"_version"`
	Result  string `json:"result"`
	Shards  struct {
		Total      int `json:"total"`
		Successful int `json:"successful"`
		Failed     int `json:"failed"`
	} `json:"_shards"`
	SeqNo       int `json:"_seq_no"`
	PrimaryTerm int `json:"_primary_term"`
}

type EsData struct {
	Took     int  `json:"took"`
	TimedOut bool `json:"timed_out"`
	Shards   struct {
		Total      int `json:"total"`
		Successful int `json:"successful"`
		Skipped    int `json:"skipped"`
		Failed     int `json:"failed"`
	} `json:"_shards"`
	Hits struct {
		Total struct {
			Value    int    `json:"value"`
			Relation string `json:"relation"`
		} `json:"total"`
		MaxScore float64 `json:"max_score"`
		Hits     []struct {
			Index  string  `json:"_index"`
			Type   string  `json:"_type"`
			ID     string  `json:"_id"`
			Score  float64 `json:"_score"`
			Source Data    `json:"_source"`
		} `json:"hits"`
	} `json:"hits"`
}

type ParamReview struct {
	Token string
	ID    string
}

type ReviewRespBL struct {
	ProductID string
	Data      []DataReview `json:"data"`
	Meta      struct {
		HTTPStatus int    `json:"http_status"`
		Offset     string `json:"offset"`
		Limit      string `json:"limit"`
		Total      int    `json:"total"`
	} `json:"meta"`
}

type DataReview struct {
	ID     int `json:"id"`
	Sender struct {
		ID        int    `json:"id"`
		Name      string `json:"name"`
		Type      string `json:"type"`
		URL       string `json:"url"`
		AvatarURL string `json:"avatar_url"`
	} `json:"sender"`
	Product struct {
		ID       string `json:"id"`
		Name     string `json:"name"`
		ImageURL string `json:"image_url"`
		URL      string `json:"url"`
	} `json:"product"`
	Review struct {
		Title   string `json:"title"`
		Content string `json:"content"`
		Rate    int    `json:"rate"`
		Anonym  bool   `json:"anonym"`
	} `json:"review"`
	Votes struct {
		PositiveVotes int    `json:"positive_votes"`
		NegativeVotes int    `json:"negative_votes"`
		UserVote      string `json:"user_vote"`
	} `json:"votes"`
	Images []struct {
		ID  int    `json:"id"`
		URL string `json:"url"`
	} `json:"images"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
